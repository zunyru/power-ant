<?php

namespace App\Providers;

use App\FormFields\DateTime;
use App\FormFields\FileuploaderMultiImage;
use App\FormFields\MyMultiSelect;
use App\FormFields\SlugFormField;
use App\FormFields\SummernoteFormField;
use App\FormFields\Url;
use Illuminate\Support\ServiceProvider;
use TCG\Voyager\Facades\Voyager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Voyager::addFormField(SummernoteFormField::class);
        Voyager::addFormField(SlugFormField::class);
        Voyager::addFormField(MyMultiSelect::class);
        Voyager::addFormField(DateTime::class);
        Voyager::addFormField(FileuploaderMultiImage::class);
        Voyager::addFormField(Url::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        seo()
            ->site(config('app.name', setting('site.title')))
            ->title(
                default: config('app.name', setting('site.title')),
                modify: fn(string $title) => $title . ' | ' . setting('site.title')
            )
            ->description(
                default: setting('site.description'),
                modify: fn(string $description) => $description)
            ->image(
                default: fn() => asset('images/banner.png'),
                modify: fn(string $image_seo) => $image_seo);
    }
}
