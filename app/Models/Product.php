<?php

namespace App\Models;

use App\Traits\SearchField;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class Product extends Model
{
    use HasFactory, SearchField, SoftDeletes,Translatable;

    protected $casts = [
        'gallery' => 'array'
    ];

    protected $searchField = [
        'name'
    ];

    protected $relationSearchField = [
        'category.name',
    ];

    protected function gallery(): Attribute
    {
        return Attribute::make(
            get: function ($value) {
                if (!is_array($value)) {
                    $value = json_decode($value);
                    if (!is_array($value)) {
                        $value = json_decode($value);
                    }
                }
                return $value;
            }
        );
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::PUBLISHED);
    }

    public function scopeIsRecommended(Builder $query, bool $recommend)
    {
        if ($recommend)
            return $query->where('is_recommended', $recommend);

        return $query;
    }

    public function scopeIsNew(Builder $query, bool $new)
    {
        //query()->is_new()
        if ($new)
            return $query->whereDate('created_at', '>=', Carbon::now()->subDays(7));

        return $query;
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
