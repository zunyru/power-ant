<?php

namespace App\Models;

use App\Traits\SearchField;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory,SoftDeletes,SearchField;

    protected $searchField = [
        'name'
    ];

    protected $relationSearchField = [
        'parentId.name',
    ];

    public $parentColumn = 'parent_id';

    public function parentId(): BelongsTo
    {
        return $this->belongsTo(self::class,$this->parentColumn);
    }

    public function children(): HasMany
    {
        return $this->hasMany(self::class, $this->parentColumn);
    }

    public function allChildren(): HasMany
    {
        return $this->children()->with('allChildren');
    }
}
