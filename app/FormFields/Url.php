<?php

namespace App\FormFields;

use TCG\Voyager\FormFields\AbstractHandler;

class Url extends AbstractHandler
{
    protected $codename = 'url';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('formfields.url', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent
        ]);
    }
}
