<?php

namespace App\Traits;

trait SearchField
{
    public function getSearchField()
    {
        return $this->searchField;
    }

    public function getRelationSearchField()
    {
        return $this->relationSearchField;
    }
}