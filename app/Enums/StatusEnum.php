<?php

namespace App\Enums;

class StatusEnum
{
    const DRAFT = 'draft';
    const PUBLISH = 'publish';

    const PUBLISHED = 'published';

    const ALL = [
        self::DRAFT,
        self::PUBLISH
    ];
}
