<?php

use App\Http\Controllers\CheckduptitleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//slug
Route::post('/check/check-dup-title/', [CheckduptitleController::class, 'checkDupTitle'])
    ->name('check.check-dup-title');

Route::post('/check/check-dup-slug/', [CheckduptitleController::class, 'checkDupSlug'])
    ->name('check.check-dup-slug');
