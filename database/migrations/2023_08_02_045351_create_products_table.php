<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->index();
            $table->string('slug')->index();
            $table->string('seo_title');
            $table->text('meta_description')->nullable();
            $table->text('meta_keyword')->nullable();

            $table->string('category_id')->nullable();
            $table->integer('price')->nullable();
            $table->integer('sell_amount')->default(0)->nullable();
            $table->boolean('is_recommended')->default(0);
            $table->longText('detail')->nullable();
            $table->string('image')->nullable();
            $table->longText('gallery')->nullable();
            $table->longText('payment_link')->nullable();
            $table->enum('status', \App\Enums\StatusEnum::ALL);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
