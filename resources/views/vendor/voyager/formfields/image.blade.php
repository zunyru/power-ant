@push('css')
    @once
        <link href="{{ asset('plugin/fileuploader/dist/font/font-fileuploader.css') }}" media="all" rel="stylesheet">
        <!-- css -->
        <link href="{{ asset('plugin/fileuploader/dist/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">
        <link rel="stylesheet"
              href="{{ asset('plugin/fileuploader/drag-drop/css/jquery.fileuploader-theme-dragdrop.css') }}">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
        <style>
            .fileuploader-theme-dragdrop .fileuploader-input {
                padding: unset;
            }
        </style>
    @endonce
@endpush

@if(isset($dataTypeContent->{$row->field}))
    <div data-field-name="{{ $row->field }}">
        <a href="#" class="voyager-x remove-single-image" style="position:absolute;"></a>
        <a target="_blank"
           href="@if( !filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}@else{{ $dataTypeContent->{$row->field} }}@endif">
        <img src="@if( !filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->{$row->field} ) }}@else{{ $dataTypeContent->{$row->field} }}@endif"
             data-file-name="{{ $dataTypeContent->{$row->field} }}" data-id="{{ $dataTypeContent->getKey() }}"
             style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
        </a>
    </div>
@endif
<input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
       name="{{ $row->field }}" accept="image/*">

@push('javascript')
    @once
        <!-- js -->
        <script src="{{ asset('plugin/fileuploader/dist/jquery.fileuploader.min.js') }}"
                type="text/javascript"></script>

        <script>
            $(document).ready(function () {
                //fileuploader
                $('input[name="{{ $row->field }}"]').fileuploader({
                    changeInput: '<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                        '<div class="fileuploader-icon-main"></div>' +
                        '<h3 class="fileuploader-input-caption"><span>${captions.feedback}</span></h3>' +
                        '<p>${captions.or}</p>' +
                        '<button type="button" class="fileuploader-input-button"><span>${captions.button}</span></button>' +
                        '</div>' +
                        '</div>',
                    theme: 'dragdrop',
                    maxSize: 2,
                    fileMaxSize: 3,
                    extensions: ['image/*'],
                    limit: 1,
                    quality: 80,
                });
            });
        </script>
    @endonce
@endpush
