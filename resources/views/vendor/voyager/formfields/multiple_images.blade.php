<br>
@push('css')
    @once
        <link href="{{ asset('plugin/fileuploader/dist/font/font-fileuploader.css') }}" media="all" rel="stylesheet">
        <!-- css -->
        <link href="{{ asset('plugin/fileuploader/dist/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">
        <link rel="stylesheet"
              href="{{ asset('plugin/fileuploader/drag-drop/css/jquery.fileuploader-theme-dragdrop.css') }}">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
        <style>
            .fileuploader-theme-dragdrop .fileuploader-input {
                padding: unset;
            }
        </style>
    @endonce
@endpush
<?php
if (is_array($dataTypeContent->{$row->field})) {
    $images = $dataTypeContent->{$row->field};
} else {
    $images = json_decode($dataTypeContent->{$row->field});
}
?>
@if(isset($images))
    @if($images != null)
        @foreach($images as $image)
            <div class="img_settings_container" data-field-name="{{ $row->field }}"
                 style="float:left;padding-right:15px;">
                <a href="#" class="voyager-x remove-multi-image" style="position: absolute;"></a>
                <img src="{{ Voyager::image( $image ) }}" data-file-name="{{ $image }}"
                     data-id="{{ $dataTypeContent->getKey() }}"
                     style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;">
            </div>
        @endforeach
    @endif
@endif
<div class="clearfix"></div>
<input @if($row->required == 1 && !isset($dataTypeContent->{$row->field})) required @endif type="file"
       name="{{ $row->field }}[]" multiple="multiple" accept="image/*" class="gallery_media">

@push('javascript')
    @once
        <!-- js -->
        <script src="{{ asset('plugin/fileuploader/dist/jquery.fileuploader.min.js') }}" type="text/javascript"></script>

        {{-- fancybox --}}
        <script type="text/javascript" src="{{ asset('plugin/fancybox/source/jquery.fancybox.pack.js') }}"></script>
        <script type="text/javascript"
                src="{{ asset('plugin/fancybox/source/helpers/jquery.fancybox-buttons.js') }}"></script>
        <script type="text/javascript"
                src="{{ asset('plugin/fancybox/source/helpers/jquery.fancybox-media.js') }}"></script>

        <script>
            $(document).ready(function () {
                $('input[name="{{ $row->field }}[]"]').fileuploader({
                    changeInput: '<div class="fileuploader-input">' +
                        '<div class="fileuploader-input-inner">' +
                        '<div class="fileuploader-icon-main"></div>' +
                        '<h3 class="fileuploader-input-caption"><span>${captions.feedback}</span></h3>' +
                        '<p>${captions.or}</p>' +
                        '<button type="button" class="fileuploader-input-button"><span>${captions.button}</span></button>' +
                        '</div>' +
                        '</div>',
                    theme: 'dragdrop',
                    maxSize: 20,
                    fileMaxSize: 3,
                    extensions: ['image/*'],
                    limit: 10,
                    quality: 80,
                });
                //fancybox
                $(".fancybox").fancybox();
            });
        </script>
    @endonce
@endpush
