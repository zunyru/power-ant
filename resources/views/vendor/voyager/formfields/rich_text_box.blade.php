<textarea class="form-control richTextBox" name="{{ $row->field }}" id="richtext{{ $row->field }}">
    {{ old($row->field, $dataTypeContent->{$row->field} ?? '') }}
</textarea>

@push('javascript')
    @once
        {{--<script src="{{ asset('plugin/prism/prism.js' }}" data-manual></script>--}}

        <script src="{{ asset('plugin/tinymce/tinymce/tinymce.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/image/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/media/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/table/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/help/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/insertdatetime/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/link/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/autolink/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/emoticons/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/emoticons/js/emojis.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/searchreplace/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/lists/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/code/plugin.min.js') }}"></script>
        <script src="{{ asset('plugin/tinymce/tinymce/plugins/codesample/plugin.min.js') }}"></script>
    @endonce

    <script>
        $(document).ready(function () {
            var additionalConfig = {
                selector: 'textarea.richTextBox[name="{{ $row->field }}"]',
                plugins: [
                    'image', 'media', 'table', 'insertdatetime', 'link', 'autolink', 'emoticons', 'searchreplace', 'lists'
                    , 'codesample', 'code', 'help'
                ],
                toolbar1: 'undo redo  | ' +
                    'bold underline italic | forecolor backcolor | removeformat | searchreplace',
                toolbar2: 'alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | numlist bullist',
                toolbar3: 'media image | link  | insertdatetime |emoticons | codesample code | help',
                images_file_types: 'jpeg,jpg,jpe,jfi,jif,jfif,png,gif,bmp,webp',
                a11y_advanced_options: true,
                image_caption: true,
                image_advtab: true,
                images_upload_credentials: true,
                link_default_protocol: 'http',
                //codesample_global_prismjs: true,
                codesample_languages: [
                    {text: 'HTML/XML', value: 'markup'},
                    {text: 'JavaScript', value: 'javascript'},
                    {text: 'CSS', value: 'css'},
                    {text: 'PHP', value: 'php'},
                    {text: 'Ruby', value: 'ruby'},
                    {text: 'Python', value: 'python'},
                    {text: 'Java', value: 'java'},
                    {text: 'C', value: 'c'},
                    {text: 'C#', value: 'csharp'},
                    {text: 'C++', value: 'cpp'}
                ],
                setup: (editor) => {
                    console.log(editor);
                    editor.ui.registry.addContextToolbar('imagealignment', {
                        predicate: (node) => node.nodeName.toLowerCase() === 'img',
                        items: 'alignleft aligncenter alignright',
                        position: 'node',
                        scope: 'node'
                    });

                    editor.ui.registry.addContextToolbar('textselection', {
                        predicate: (node) => !editor.selection.isCollapsed(),
                        items: 'bold underline italic | blockquote | backcolor',
                        position: 'selection',
                        scope: 'node'
                    });
                },
            }

            $.extend(additionalConfig, {!! json_encode($options->tinymceOptions ?? (object)[]) !!})

            tinymce.init(window.voyagerTinyMCE.getConfig(additionalConfig));
        });
    </script>
@endpush
