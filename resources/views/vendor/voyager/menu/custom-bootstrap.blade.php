@php
    if(!isset($innerLoop)){
    $menus = menu('admin', '_json');
    }
    if (Voyager::translatable($items)) {
    $items = $items->load('translations');
    $menus = $menus->load('translations');
    }
@endphp
<ul class="nav navbar-nav">
    @foreach ($menus as $item)
        @php
            $order_count = App\Models\Order::query()
            ->where('payment', 'transfer')
            ->where('status','pending')
            ->count();

            $work_count = App\Models\Homework::query()
            ->where('is_open', false)
            ->whereNull('parent_id')
            ->count();

            $originalItem = $item;
            if (Voyager::translatable($item)) {
            $item = $item->translate($options->locale);
            }
            $listItemClass = null;
            $linkAttributes = null;
            $styles = null;
            $icon = null;
            $caret = null;
            $dropdown = null;
            $open = null;
            // Background Color or Color
            if (isset($item->color) && $item->color == true) {
            $styles = 'color:'.$item->color;
            }
            if (isset($item->background) && $item->background == true) {
            $styles = 'background-color:'.$item->color;
            }
            $segments_sub = array_filter(explode('/', str_replace(route('voyager.dashboard'), '', Request::url())));
            $segments = array_filter(explode('/', str_replace(route('voyager.dashboard'), '', $item->link())));
            if(url($item->link()) == url()->current()){
            $listItemClass = 'active';
            }else{
            $listItemClass = '';
            }
            if(sizeof($segments_sub) > 0){
            foreach ($segments as $key => $segment) {
            if($segment == $segments_sub[1]){
            $listItemClass = 'active';
            }
            }
            }
            if (!$item->children->isEmpty()) {
            $dropdown = 'dropdown';
            }
            // Set Icon
            $icon = get_icon($item->icon_class);
        @endphp
        <li class="{{ $listItemClass }} {{ $dropdown }}">
            <a target="{{ $item->target }}"
               href="{{ !$item->children->isEmpty() ? '#'.$item->id.'-dropdown-element' : url($item->link()) }}" {!!
            $linkAttributes ?? '' !!} {!! !$originalItem->children->isEmpty() ? 'data-toggle="collapse"
            aria-expanded="false" class="collapsed"' : '' !!}>
                {!! $icon !!}
                <span class="title">
                    {{ $item->title }}
                    @if($item->route == 'voyager.orders.index')
                        @if($order_count)
                            <span class="new-icon">{{ $order_count }}</span>
                        @endif
                    @endif
                    @if($item->route == 'voyager.members.index')
                        @if($work_count)
                            <span class="new-icon">{{ $work_count }}</span>
                        @endif
                    @endif
                </span>

            </a>
            @if(!$item->children->isEmpty())
                <div id="{{ $item->id.'-dropdown-element' }}" class="collapse">
                    <div class="panel-body">
                        @include('voyager::menu.custom-bootstrap', ['menus' => $originalItem->children, 'options' => $options,
                        'innerLoop' => true ])
                    </div>
                </div>
            @endif
        </li>
    @endforeach
</ul>
