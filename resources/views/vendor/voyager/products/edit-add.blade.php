@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::master')

@section('css')

@stop

@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular'))

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content container-fluid">
        <!-- form start -->
        <form role="form"
              class="form-edit-add"
              action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
              method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->
            @if($edit)
                <input type="hidden" id="edit_" value="{{ $dataTypeContent->getKey() }}">
                {{ method_field("PUT") }}
            @endif

            <!-- CSRF TOKEN -->
            {{ csrf_field() }}

            <div class="panel-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <!-- Adding / Editing -->
                @php
                    $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                @endphp

                <div class="row">
                    <div class="col-md-8">

                        <!-- ### TITLE ### -->
                        <div class="panel">

                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    Product Name
                                    <span class="panel-desc"> </span>
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                       aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body" id="title_page">
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'name',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'name')
                                ])
                                <input type="text" class="form-control"
                                       id="name"
                                       name="name"
                                       required
                                       placeholder="Product Name"
                                       value="{{ $dataTypeContent->name ?? '' }}">
                            </div>
                        </div>


                        <!-- ### DETAIL ### -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">Detail</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen"
                                       aria-hidden="true"></a>
                                </div>
                            </div>

                            <div class="panel-body">
                                @include('voyager::multilingual.input-hidden', [
                                    '_field_name'  => 'detail',
                                    '_field_trans' => get_field_translations($dataTypeContent, 'detail')
                                ])
                                @php
                                    $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                                    $row = $dataTypeRows->where('field', 'detail')->first();
                                @endphp
                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                            </div>
                        </div><!-- .panel -->

                        <!-- ### GALLERY ### -->
                        <div class="panel panel-bordered panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-image"></i> {{ 'Gallery' }}
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                       aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    @php
                                        $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                                        $row = $dataTypeRows->where('field', 'gallery')->first();
                                    @endphp
                                    {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="real_price">Sell Amount</label>
                            <input type="number" class="form-control"
                                   id="sell_amount"
                                   name="sell_amount"
                                   placeholder="Sell Amount"
                                   value="{{ $dataTypeContent->sell_amount ?? 0 }}">
                        </div>


                    </div>

                    <div class="col-md-4">
                        <!-- ### DETAILS ### -->
                        <div class="panel panel panel-bordered panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i
                                        class="icon wb-clipboard"></i> {{ __('voyager::post.details') }}
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                       aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="slug">Product slug</label>
                                    @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'slug',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                    ])
                                    @include('voyager::multilingual.input-hidden', [
                                        '_field_name' => 'slug',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                                     ])
                                    {{--<input type="text" class="form-control" id="slug" name="slug"
                                        placeholder="slug"
                                        {!! isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug") !!}
                                        value="{{ $dataTypeContent->slug ?? '' }}">--}}
                                    <input type="text" class="form-control" id="slug" name="slug" placeholder="slug" {!!
                                isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug" ) !!}
                                    value="{{ $dataTypeContent->slug ?? '' }}" data-db="{{ $dataType->name }}">
                                    <label id="slug-error-dup" class="dup-error" for="{{ "slug" }}"
                                           style="display: none;color:red">{{ "slug ซ้ำ" }}</label>
                                    @push('javascript')
                                        @include('javascript.slug-js');
                                    @endpush
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select class="form-control" name="status">
                                        <option value="PUBLISHED"
                                                @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'PUBLISHED') selected="selected"@endif>{{ __('voyager::post.status_published') }}</option>
                                        <option value="DRAFT"
                                                @if(isset($dataTypeContent->status) && $dataTypeContent->status == 'DRAFT') selected="selected"@endif>{{ __('voyager::post.status_draft') }}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="category_id">Category</label>

                                    {{--@php
                                        $dataTypeRows = $dataType->{$edit ? 'editRows' : 'addRows'};
                                        $row = $dataTypeRows->where('field', 'category_belongsto_category_relationship')->first();
                                    @endphp
                                    @if ($row->type == 'relationship')
                                        @include('voyager::formfields.relationship', [
                                            'options' => $row->details,
                                        ])
                                    @else
                                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                    @endif--}}

                                    <select class="form-control" name="category_id">
                                        <option value="">--Selet Category--</option>
                                        @foreach(\App\Models\Category::all() as $category)
                                            <option value="{{ $category->id }}"
                                                    @if(isset($dataTypeContent->category_id) && $dataTypeContent->category_id == $category->id) selected="selected"@endif>{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>


                                <div class="form-group">
                                    <label for="price">Price</label>
                                    <input type="number" class="form-control"
                                           id="price"
                                           name="price"
                                           placeholder="Price"
                                           value="{{ $dataTypeContent->price ?? '' }}">
                                </div>


                                <div class="form-group">
                                    <input id="is_recommended" type="checkbox" name="is_recommended"
                                           @if(isset($dataTypeContent->is_recommended) && $dataTypeContent->is_recommended) checked="checked"@endif>
                                    <label for="is_recommended">Recommended</label>
                                </div>
                            </div>
                        </div>

                        <!-- ### IMAGE ### -->
                        <div class="panel panel-bordered panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-image"></i> {{ 'Product Image' }}
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                       aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    @php
                                        $dataTypeRows = $dataType->{($edit ? 'editRows' : 'addRows' )};
                                        $row = $dataTypeRows->where('field', 'image')->first();
                                    @endphp
                                    {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                </div>

                            </div>
                        </div>

                        <!-- ### SEO CONTENT ### -->
                        <div class="panel panel-bordered panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i
                                        class="icon wb-search"></i> {{ __('voyager::post.seo_content') }}
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                       aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label for="seo_title">{{ 'SEO title' }}</label>
                                    @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'seo_title',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'seo_title')
                                    ])
                                    <input type="text" class="form-control" name="seo_title" placeholder="SEO Title"
                                           value="{{ $dataTypeContent->seo_title ?? '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="meta_description">{{ 'Meta description' }}</label>
                                    @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'meta_description',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'meta_description')
                                    ])
                                    <textarea class="form-control"
                                              name="meta_description">{{ $dataTypeContent->meta_description ?? '' }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="meta_keywords">{{ 'Meta keywords' }}</label>
                                    @include('voyager::multilingual.input-hidden', [
                                        '_field_name'  => 'meta_keywords',
                                        '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords')
                                    ])
                                    <textarea class="form-control"
                                              name="meta_keywords">{{ $dataTypeContent->meta_keywords ?? '' }}</textarea>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div><!-- panel-body -->

            <div class="panel-footer">
                @section('submit-buttons')
                    <button type="submit"
                            class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                @stop
                @yield('submit-buttons')
            </div>
        </form>

        <div style="display:none">
            <input type="hidden" id="upload_url" value="{{ route('voyager.upload') }}">
            <input type="hidden" id="upload_type_slug" value="{{ $dataType->slug }}">
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;
                    </button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                            id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')

    <script>
        var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
            return function () {
                $file = $(this).siblings(tag);

                params = {
                    slug: '{{ $dataType->slug }}',
                    filename: $file.data('file-name'),
                    id: $file.data('id'),
                    field: $file.parent().data('field-name'),
                    multi: isMulti,
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text(params.filename);
                $('#confirm_delete_modal').modal('show');
            };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            $("input[name='file_type']").change(function () {
                if ($(this).val() == 'link') {
                    $('.file_type_link').show();
                    $('.file_type_file').hide();
                } else {
                    $('.file_type_file').show();
                    $('.file_type_link').hide();
                }
            });

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                } else if (elt.type != 'date') {
                    elt.type = 'text';
                    $(elt).datetimepicker({
                        format: 'L',
                        extraFormats: ['YYYY-MM-DD']
                    }).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
            $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function (i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function () {
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if (response
                        && response.data
                        && response.data.status
                        && response.data.status == 200) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function () {
                            $(this).remove();
                        })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        })
        ;

    </script>
@stop
