<div>
</div>
@once
    <script src="{{ asset('js/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
@endonce
<script>

    const image_upload_handler = (blobInfo, progress) => new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.withCredentials = true;
        xhr.open("POST", "{{ route('comment.upload.image') }}");
        var token = '{{ csrf_token() }}';
        xhr.setRequestHeader("X-CSRF-Token", token);
        xhr.upload.onprogress = (e) => {
            progress(e.loaded / e.total * 100);
        };

        xhr.onload = () => {
            if (xhr.status === 403) {
                reject({ message: "HTTP Error: " + xhr.status, remove: true });
                return;
            }

            if (xhr.status < 200 || xhr.status >= 300) {
                reject("HTTP Error: " + xhr.status);
                return;
            }

            const json = JSON.parse(xhr.responseText);

            if (!json || typeof json.location != "string") {
                reject("Invalid JSON: " + xhr.responseText);
                return;
            }

            resolve(json.location);
        };

        xhr.onerror = () => {
            reject("Image upload failed due to a XHR Transport error. Code: " + xhr.status);
        };

        const formData = new FormData();
        formData.append("file", blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
    });


    tinymce.init({
        selector: "textarea#myeditorinstance",
        //plugins: "code table lists",
        //toolbar: "undo redo | blocks | bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table"
        menubar: false,
        width: "100%",
        height: 500,
        forced_root_block: false,
        force_br_newlines: true,
        force_p_newlines: false,
        toolbar: "undo redo | bold italic underline | link | numlist bullist | image emoticons | searchreplace",
        plugins: "nonbreaking link searchreplace lists image emoticons",
        media_live_embeds: true,
        //images_upload_url: '{{-- route('upload.image') --}}',
        images_upload_handler: image_upload_handler,
        images_upload_credentials: true,
        file_picker_types: "file image media",
        convert_urls: false,
        placeholder: "รายละเอียดกระทู้..."
    });
</script>
