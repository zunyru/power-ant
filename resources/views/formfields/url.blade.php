<input type="url"
       class="form-control url"
       name="{{ $row->field }}"
       pattern="https://.*"
       @if($row->required == 1) required @endif
       placeholder="{{ $row->getTranslatedAttribute('display_name') }}"
       value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@else{{old($row->field)}}@endif">
