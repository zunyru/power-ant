{{--@isset($dataTypeContent->body)--}}
@push('css')
    @once
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
    @endonce
@endpush
@push('javascript')
    @once
        {{-- <script src="{!!url('/js/jquery-3.6.1.min.js')!!}"></script>--}}
        <script src="{!!url('/plugin/summernote/custom.js')!!}"></script>
        <script>
            $(document).ready(function () {
                console.log('init summernote');
                //summernote
                $('.summernote').summernote({
                    fontNames: ['Arial', 'Arial Black', 'Comic Sans MS', 'Courier New', 'Helvetica', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana', 'Roboto', 'Angsana New'],
                    fontNamesIgnoreCheck: ['Angsana New'],
                    fontSizes: ['10', '11', '12', '14', '16', '18', '20', '22', '24', '36', '48', '64', '82',
                        '100'
                    ],
                    height: 400,
                    tabsize: 2,
                    toolbar: [
                        ['style'],
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['font', ['strikethrough', 'superscript', 'subscript']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['link'],
                        ['table'],
                        ['media', ['picture', 'video']],
                        ['hr'],
                        ['misc', ['fullscreen', 'codeview', 'undo', 'redo', 'help']]
                    ],
                    callbacks: {
                        onImageUpload: function (files) {
                            sendFile(files);
                        },
                        onChange: function (contents, $editable) {
                            console.log('onChange:', contents, $editable);
                            clean_html(this, "b", contents);
                            clean_html(this, "i", contents);
                        }

                    },
                    codemirror: { // codemirror options
                        theme: 'monokai'
                    },

                });
                $('.summernote').summernote('code', `{!! $dataTypeContent->body !!}`);

                function clean_html(editor, type, value) {
                    if (value.indexOf("<" + type + ">") >= 0) {
                        if (type == "b") {
                            marca = /<b(?:.*?)>(?:.*?)<\/b>/g;
                            replaceIniTag = "<strong>";
                            replaceEndTag = "</strong>";
                        } else {
                            marca = /<i(?:.*?)>(?:.*?)<\/i>/g;
                            replaceIniTag = "<em>";
                            replaceEndTag = "</em>";
                        }
                        var matches = value.match(marca),
                            len = matches.length,
                            i;
                        for (i = 0; i < len; i++) {
                            str = $(matches[i]).text();
                            str = replaceIniTag + str + replaceEndTag;
                            value = value.replace(matches[i], str);
                        }
                        $(editor).code(value); //Replace the editor content
                    }
                }

            });

        </script>
    @endonce
@endpush
{{--@endisset--}}
<textarea class="summernote" name="{{ $row->field }}" data-name="{{ $row->display_name }}"
          @if($row->required == 1) required @endif
          step="any"
          placeholder="{{ isset($options->placeholder)? old($row->field, $options->placeholder): $row->display_name }}"
          value="@if(isset($dataTypeContent->{$row->field})){{ old($row->field, $dataTypeContent->{$row->field}) }}@else{{old($row->field)}}@endif">
</textarea>
